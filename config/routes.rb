Rails.application.routes.draw do
  # apiコントローラのルーティング
  namespace :api, { format: 'json' } do
    namespace :v1 do
      # デバイスコントローラはデバイスの名前の取得とレコードの登録に使う
      resources :devices, :only => [:index, :show, :create, :destroy] do
        collection do
          post ':id/records', to: 'devices#create_records'
        end
      end
      # レコードコントローラは読み取り専用，レコードの登録はデバイスコントローラから
      resources :records, :only => [:index, :show]
      # マップコントローラ
      resources :maps, :only => [:index, :show, :create] do
        collection do
          get 'download', to: 'maps#download'
        end
      end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
    end
  end
  resources :maps
  resources :devices, :only => :show

end
