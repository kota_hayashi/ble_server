json.extract! device, :id, :name, :created_at, :updated_at
json.url api_v1_device_url(device, format: :json)
