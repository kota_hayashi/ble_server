module Api
  module V1
    class RecordsController < ApplicationController
      before_action :set_record, only: [:show, :edit, :update, :destroy]

      # GET /records.json
      def index
        @records = Record.all
      end

      # GET /records/1.json
      def show
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_record
          @record = Record.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def record_params
          params.require(:record).permit(:time, :major, :rssi, :device_id)
        end
    end
  end
end
