module Api
  module V1
    class DevicesController < ApplicationController
      before_action :set_device, only: [:show, :destroy, :create_records]
      protect_from_forgery :except => [:create, :create_records]
      # GET /api/v1/devices.json
      def index
        @devices = Device.all
      end

      # GET /api/v1/devices/1.json
      def show
      end

      # POST /api/v1/devices.json
      def create
        device_params_tmp = device_params
        # すでにある名前かチェック
        @device = Device.find_by(name: device_params_tmp[:name])
        if @device.nil?
          # 新しい名前の場合の処理
          @device = Device.new(device_params_tmp)
          respond_to do |format|
            if @device.save
              format.html { redirect_to @device, notice: 'Device was successfully created.' }
              format.json { render :show, status: :created, location: @device }
            else
              format.html { render :new }
              format.json { render json: @device.errors, status: :unprocessable_entity }
            end
          end
        else
          # すべにある場合の処理
          render :show, status: :created, location: @device
        end
      end

      # DELETE /api/v1/devices/1.json
      def destroy
        @device.destroy
        respond_to do |format|
          format.html { redirect_to devices_url, notice: 'Device was successfully destroyed.' }
          format.json { head :no_content }
        end
      end

      # POST /api/v1/devices/1/records.json
      def create_records
        params[:device][:records_attributes].each do |record_params|
          logger.debug record_params
          record = Record.new(record_params.permit(:time, :major, :rssi))
          record[:device_id] = @device.id
          record.save
        end
        render :json => @device
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_device
          @device = Device.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def device_params
          params.require(:device).permit(:name, records_attributes: [:time, :major, :rssi])
        end

    end
  end
end
