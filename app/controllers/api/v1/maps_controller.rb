require 'base64'
module Api
  module V1
    class MapsController < ApplicationController
      before_action :set_map, only: [:show, :edit, :update, :destroy]
      protect_from_forgery :except => [:create]
      # GET /api/v1/maps
      # GET /api/v1/maps.json
      def index
        unless params[:major].blank?
          # major値を指定することで，そのmajor値を持つビーコンが存在するフロア情報をまとめて返す．
          beacon = Beacon.find_by(major: params[:major])
          unless beacon.nil?
            map = Map.find(beacon.map_id)
            payload = {}
            payload[:is_exist] = "true"
            payload[:map_id] = beacon.map_id
            payload[:beacons] = map.beacons.select(:major, :x, :y, :id)
            render :json => payload
          else
            payload = {}
            payload[:is_exist] = "false"
            render :json => payload
          end
        else
          @maps = Map.all
        end
      end

      # GET /api/v1/maps/1
      # GET /api/v1/maps/1.json
      def show
      end

      # GET /api/v1/maps/new
      def new
        @map = Map.new
      end

      # GET /api/v1/maps/1/edit
      def edit
      end

      # POST /api/v1/maps.json
      def create
        @map = Map.new(map_params)

        respond_to do |format|
          if @map.save
            format.html { redirect_to @map, notice: 'Map was successfully created.' }
            format.json { render :show, status: :created, location: @map }
          else
            format.html { render :new }
            format.json { render json: @map.errors, status: :unprocessable_entity }
          end
        end
      end

      # PATCH/PUT /api/v1/maps/1.json
      def update
        respond_to do |format|
          if @map.update(map_params)
            format.html { redirect_to @map, notice: 'Map was successfully updated.' }
            format.json { render :show, status: :ok, location: @map }
          else
            format.html { render :edit }
            format.json { render json: @map.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /api/v1/maps/1.json
      def destroy
        @map.destroy
        respond_to do |format|
          format.html { redirect_to maps_url, notice: 'Map was successfully destroyed.' }
          format.json { head :no_content }
        end
      end

      # GET /api/v1/maps/download.json
      def download
        # IDかNAMEをパラメータで指定する
        if not params[:id].blank?
          @map = Map.find(params[:id])
        elsif not params[:name].blank?
          @map = Map.find_by(name: params[:name])
        else
          render :json => {}
        end

        if @map.nil?
          render :json => {}
        else
          payload = {}
          encoded_string = Base64.encode64(File.open(@map.image.path, "rb").read)
          payload[:base64] = encoded_string
          payload[:name] = @map.name
          render :json => payload
        end
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_map
          @map = Map.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def map_params
          params.require(:map).permit(:name, :image, :image_cache, :remove_image)
        end
    end
  end
end
