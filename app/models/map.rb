class Map < ApplicationRecord
  mount_base64_uploader :image, ImageUploader
  has_many :beacons
end
