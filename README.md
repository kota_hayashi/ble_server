# BLEサーバーのアプリケーション

## 使い方

このアプリケーションを動かす場合は、まずはリポジトリを手元にクローンしてください。
その後、次のコマンドで必要になる RubyGems をインストールします。

```
$ bundle install --without production
```

その後、データベースへのマイグレーションを実行します。

```
$ rails db:migrate
```

最後に、テストを実行してうまく動いているかどうか確認してください。

```
$ rails test
```

テストが無事に通ったら、Railsサーバーを立ち上げる準備が整っているはずです。

```
$ rails server -p ポート名
```

APIの使い方

1. デバイス登録 and デバイスIDの取得  
post host:port/devices.json
1. レコードの登録  
post host:port/devices/:id/records.json
1. マップ画像のダウンロード  
get host:port/maps/download.json?id="id"
or
get host:port/maps/download.json?name="name"

