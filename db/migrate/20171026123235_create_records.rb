class CreateRecords < ActiveRecord::Migration[5.1]
  def change
    create_table :records do |t|
      t.datetime :time
      t.integer :major
      t.integer :rssi
      t.references :device, foreign_key: true

      t.timestamps
    end
  end
end
