class CreateBeacons < ActiveRecord::Migration[5.1]
  def change
    create_table :beacons do |t|
      t.integer :major
      t.float :x
      t.float :y
      t.references :map, foreign_key: true

      t.timestamps
    end
  end
end
