# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# サンプルロボットデータ
Device.create!(name: "kiyo=san")
Beacon.create!(major: 220,
                x: 1.0,
                y: 1.0,
              map_id: 18)
Beacon.create!(major: 221,
                x: 1.0,
                y: 1.0,
              map_id: 18)
Beacon.create!(major: 222,
                x: 1.0,
                y: 1.0,
              map_id: 18)
